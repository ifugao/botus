include config.mk
.PHONY: all uninstall install clean

all: botus-irc botus-discord

botus-irc: script.c
	$(CC) -o $@ $<

botus-discord: script.c
	$(CC) -o $@ $< -DDISCORD

clean:
	rm -f botus-irc botus-discord

install: ${TARGETS}
	cp botus-irc botus-discord ${DESTDIR}${BINDIR}/

uninstall:
	rm -f ${DESTDIR}${BINDIR}/botus-irc ${DESTDIR}${BINDIR}/botus-discord
