#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <locale.h>

#define bool char
#define true (1)
#define false (0)
#define SAVEFILE "score.txt"
#define WORDCOUNT (13228408)

typedef struct Trie
{
	bool estMot;
	int taille;
	struct Trie* fils[36];
} Trie;

Trie* createTrie()
{
	Trie* node = malloc(sizeof(Trie));
	node->estMot=false;
	node->taille=0;
	for(int i=0;i<36;i++)
		node->fils[i]=NULL;
	return node;
}

char buffer[1000],mot[100],cherche[36],trouve[36],lettres[36],indication[36];
bool tested[36][36], ispresent[36], foundcount[36], knownpresent[36];
char nick[100],st[100],moi[100],truc[100];

#define NBACCENTS (14)
char* accents[NBACCENTS]={"é","è","ê","ë","â","ä","à","ü","û","ù","î","ï","ô","ö"};
char sansaccents[NBACCENTS]={'e','e','e','e','a','a','a','u','u','u','i','i','o','o'};
int lenaccents[NBACCENTS]={-1+sizeof("é"),-1+sizeof("è"),-1+sizeof("ê"),-1+sizeof("ë"),
-1+sizeof("â"),-1+sizeof("ä"),-1+sizeof("à"),-1+sizeof("ü"),-1+sizeof("û"),-1+sizeof("ù"),
-1+sizeof("î"),-1+sizeof("ï"),-1+sizeof("ô"),-1+sizeof("ö")};

FILE *out, *dico;
int in;
Trie* racinePropose;

void ajoutTrie(Trie* u,char* s)
{
	for(int k=0;s[k]!='\0';k++)
	{
		u->taille++;
		int id;
		if(isalpha(s[k]))
			id=s[k]-'A';
		else
			id=s[k]-'0'+26;
		if(u->fils[id]==NULL)
			u->fils[id]=createTrie();
		u=u->fils[id];
	}
	u->taille++;
	u->estMot=true;
}

void libereTrie(Trie* u)
{
	for(int i=0;i<36;i++)
		if(u->fils[i]!=NULL)
			libereTrie(u->fils[i]);
	free(u);
}

bool chercheTrie(Trie* u,char* s)
{
	for(int k=0;s[k]!='\0';k++)
	{
		int id;
		if(isalpha(s[k]))
			id=s[k]-'A';
		else
			id=s[k]-'0'+26;
		if(0<=id && id<36 && u->fils[id]!=NULL)
			u=u->fils[id];
		else
			return false;
	}
	return u->estMot;
}

bool estPresent(char* s)
{
	bool found=false;
	dico = fopen("dico.txt","r");
	while(fscanf(dico,"%s",buffer)!=EOF && !found)
	{
		found|= !strcmp(buffer,s);
	}
	fclose(dico);
	return found;
}

void piocheMot(char* s,int k)
{
	dico = fopen("wordlist.txt","r");
	while(k-->0)
	{
		fscanf(dico,"%s",s);
	}
	fclose(dico);
}

typedef struct score_t
{
	char* nick;
	long long score;
	unsigned int count;
} score_t;

typedef struct pascal_score_t
{
	struct score_t* data;
	size_t length, max_length;
} pascal_score_t;

pascal_score_t* scores;

pascal_score_t* createScore()
{
	pascal_score_t* scores = malloc(sizeof(pascal_score_t));
	scores->length = 0;
	scores->max_length = 8;
	scores->data = malloc(scores->max_length * sizeof(score_t));
	return scores;
}

void appendScore(pascal_score_t* scores, long long x, int c){
	if(scores->length >= scores->max_length)
	{
		score_t* data = malloc(2*sizeof(score_t)*scores->max_length);
		memcpy(data, scores->data, sizeof(score_t)*scores->max_length);
		scores->max_length = scores->max_length*2;
		free(scores->data);
		scores->data = data;
	}
	scores->data[scores->length].nick=malloc(strlen(nick)+1);
	strcpy(scores->data[scores->length].nick, nick);
	scores->data[scores->length].score = x;
	scores->data[scores->length].count = c;
	scores->length++;
}

void addScore(pascal_score_t* scores, long long x, int c)
{
	for(int i = 0 ; i < scores->length ; ++i)
	{
		if(!strcmp(nick, scores->data[i].nick))
		{
			scores->data[i].score += x;
			scores->data[i].count += c;
			return;
		}
	}
	appendScore(scores, x, c);
}

pascal_score_t* loadScore(char* path)
{
	pascal_score_t* scores = createScore();
	FILE* scorefile = fopen(path, "r");
	if(!scorefile) return NULL;
	while(fscanf(scorefile,"%s",nick)!=EOF)
	{
		long long score; unsigned int count;
		fscanf(scorefile,"%lld %u",&score,&count);
		appendScore(scores, score, count);
	}
	fclose(scorefile);
	return scores;
}

void saveScore(char* path, pascal_score_t* scores)
{
	FILE* scorefile=fopen(path,"w");
	for(int i = 0 ; i < scores->length ; ++i)
		fprintf(scorefile,"%s %lld %u\n",scores->data[i].nick,scores->data[i].score,scores->data[i].count);
	fclose(scorefile);
}

int cmp(const void* a, const void* b)
{
	long long v=((score_t*)b)->score-((score_t*)a)->score;
	if(v>0)
		return 1;
	else if(v<0)
		return -1;
	else return 0;
}

void printScore(pascal_score_t* scores)
{
	qsort(scores->data,scores->length,sizeof(score_t),cmp);
	for(int i = 0 ; i < scores->length && i < 10 ; ++i)
	{
		fprintf(out,"_%s: %lld(%u)",scores->data[i].nick,scores->data[i].score,scores->data[i].count);
		if(i < scores->length - 1 && i < 9) fprintf(out," | ");
		else fprintf(out,"\n");
	}
	fflush(out);
}

bool mygets(char* b,size_t sz,int fd)
{
	size_t i=0;
	char c='a';
	while(c!='\n' && i < sz)
	{
		if(read(fd,&c,sizeof(char))!=sizeof(char))
			return false;
		b[i++]=c;
	}
	b[i-1]='\0';
	return true;
}

void partie()
{
	srand(time(NULL));
	piocheMot(cherche,rand()%WORDCOUNT);
	int lenCherche=strlen(cherche);
	memset(tested,0,sizeof(tested));
	memset(ispresent,0,sizeof(ispresent));
	for(int i=0;cherche[i]!='\0';i++)
		if(isalpha(cherche[i]))
			ispresent[cherche[i]-'A']=true;
		else
			ispresent[cherche[i]-'0'+26]=true;
	memset(foundcount,0,sizeof(foundcount));
	memset(knownpresent,0,sizeof(knownpresent));
	fprintf(out,"Mot en %d lettres\n",lenCherche);
#ifdef DISCORD
	fprintf(out,"`");
#endif
	for(int i=0;cherche[i]!='\0';i++)
		fprintf(out,"ˍ");
#ifdef DISCORD
	fprintf(out,"`");
#endif
	fprintf(out,"\n");
	fflush(out);
	racinePropose=createTrie();
	while(1)
	{	
		if(!mygets(buffer,1000,in))
		{
			sleep(0.5);
			continue;
		}
		if(sscanf(buffer,"%s%s%s%s",st,nick,moi,mot)<4) continue;
		if(strncmp(moi,"botus",5)!=0)
			continue;
		for(int i=1;nick[i]!='\0';i++)
			nick[i-1]=(nick[i]!='>'?nick[i]:'\0');
#ifdef DISCORD
		char* ps=strrchr(nick,'_');
		if(ps!=NULL)
			*ps='\0';
#endif
		int k=0;
		for(int j=0;mot[j]!='\0';j++){
			bool flag=false;
			for(int idchar=0;idchar<NBACCENTS;idchar++)
				if(!strncmp(mot+j,accents[idchar],lenaccents[idchar]))
				{
					truc[k++]=sansaccents[idchar];
					j+=lenaccents[idchar]-1;
					flag=true;
					break;
				}
			if(!flag)
				truc[k++]=mot[j];
		}
		truc[k]='\0';
		strcpy(mot,truc);
		if(mot[0]=='!'){
			if(strncmp(mot,"!indice",7)==0){
				int k;
				do{
					k=rand()%lenCherche;
				}while(trouve[k]);
				trouve[k]=true;
			}
			/*else if(strncmp(mot,"!suivant",8)==0){
				piocheMot(racineChoix,cherche,rand()%racineChoix->taille);
				lenCherche=strlen(cherche);
				fprintf(out,"Mot en %d lettres\n",lenCherche);
				memset(trouve,0,26);
				libereTrie(racinePropose);
				racinePropose=createTrie();
			}*/
			else if(strncmp(mot,"!stop",5)==0){
				break;
			}
			else if(strncmp(mot,"!help",5)==0){
				fprintf(out,"Les différentes commandes sont \"!indice\",\"!suivant\",\"!stop\"\n");
			}
			else if(strncmp(mot,"!scores",7)==0){
				printScore(scores);
			}
		}
		else if(strlen(mot)==lenCherche){
			bool flagskip=false;
			for(int i=0;mot[i]!='\0';i++)
				if(!isalnum(mot[i]))
					flagskip=true;
			if(flagskip) continue;
			for(int i=0;mot[i]!='\0';i++)
				if(isalpha(mot[i]))
					mot[i]=toupper(mot[i]);
			if(estPresent(mot))
			{
				if(!chercheTrie(racinePropose,mot))
				{
					int scoreGain=0;
					memset(lettres,0,36);
					int nbCorrectes=0;
					for(int i=0;mot[i]!='\0';i++)
						if(mot[i]==cherche[i])
						{
							if(!trouve[i])
							{
								trouve[i]=true;
								scoreGain+=5;
							}
							nbCorrectes++;
						}
						else
						{
							int codeLettre=(isalpha(mot[i])?mot[i]-'A':mot[i]-'0'+26);
							if(!trouve[i] && !tested[i][codeLettre] && ispresent[codeLettre])
							{
								tested[i][codeLettre]=true;
								scoreGain+=1;
							}
							if(ispresent[codeLettre] && !knownpresent[codeLettre])
							{
								knownpresent[codeLettre]=true;
								scoreGain+=1;
							}
							codeLettre=(isalpha(cherche[i])?cherche[i]-'A':cherche[i]-'0'+26);
							lettres[codeLettre]++;
						}
					memset(indication,'_',25);
#ifdef DISCORD
					fprintf(out,"`");
#else
					fprintf(out,"%c",0x02);
#endif
					for(int i=0;mot[i]!='\0';i++)
					{
						if(mot[i]==cherche[i])
						{
							//printf("\e[1;32m%c\e[0m",mot[i]);
							fprintf(out,"%c03%c%c00",0x03,mot[i],0x03);
							indication[i]='^';
						}
						else if(lettres[(isalpha(mot[i])?mot[i]-'A':mot[i]-'0'+26)]>0)
						{
							//printf("\e[1;31m%c\e[0m",mot[i]);
							fprintf(out,"%c05%c%c00",0x03,mot[i],0x03);
							lettres[(isalpha(mot[i])?mot[i]-'A':mot[i]-'0'+26)]--;
							indication[i]='~';
						}
						else
						{
							fprintf(out,"%c00%c",0x03,mot[i]);
							int codeLettre=(isalpha(mot[i])?mot[i]-'A':mot[i]-'0'+26);
							if(!foundcount[codeLettre])
							{
								foundcount[codeLettre]=true;
								scoreGain+=1;
							}
						}
					}
					if(nbCorrectes==lenCherche) scoreGain+=5;
#ifndef DISCORD
					fprintf(out,"%c (%d pts)\n",0x02,scoreGain);
#else
					fprintf(out,"` (%d pts)\n`",scoreGain);
					for(int i=0;mot[i]!='\0';i++)
					{
						fprintf(out,"%c",indication[i]);
					}
					fprintf(out,"` ");
#endif
					ajoutTrie(racinePropose,mot);
					addScore(scores,scoreGain,(nbCorrectes==lenCherche));
					if(nbCorrectes==lenCherche)
					{
						fprintf(out,"Bravo %s !\n",nick);
						fflush(out);
						libereTrie(racinePropose);
						memset(trouve,0,36);
						break;
					}
				}
				else
				{
					fprintf(out,"Mot déjà proposé ! (-3 pts)\n");
					addScore(scores,-3,0);
					fflush(out);
				}
			}
			else
			{
				fprintf(out,"Mot invalide ! (-10 pts)\n");
				addScore(scores,-10,0);
				fflush(out);
			}
		}
		else
		{
			fprintf(out,"Longueur incorrecte : %d\n",strlen(mot));
			fflush(out);
		}
#ifdef DISCORD
		fprintf(out,"`");
#endif
		for(int i=0;cherche[i]!='\0';i++)
			if(trouve[i])
				fprintf(out,"%c",cherche[i]);
			else
				fprintf(out,"ˍ");
#ifdef DISCORD
		fprintf(out,"`");
#endif
		fprintf(out,"\n");
		fflush(out);
	}
	saveScore(SAVEFILE,scores);
}

int main()
{
	setlocale(LC_ALL, "");
	out = fopen("in","w");
	fflush(out);
	in=open("out",O_RDONLY | O_NONBLOCK);
	assert(in!=-1);
	assert(out!=NULL);
	scores=loadScore(SAVEFILE);
	lseek(in,-1,SEEK_END);
	while(1)
	{
		if(mygets(buffer,1000,in))
		{
			if(strstr(buffer,"botus: !start")!=0||strstr(buffer,"botus: encore")!=0)
				partie();
			if(strstr(buffer,"botus: !scores")!=0)
				printScore(scores);
		}
		sleep(1);
	}
}
