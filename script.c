#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <locale.h>

#define bool char
#define true (1)
#define false (0)
#define SAVEFILE "score.txt"

typedef struct Trie
{
	bool estMot;
	int taille;
	struct Trie* fils[26];
} Trie;

Trie* createTrie()
{
	Trie* node = malloc(sizeof(Trie));
	node->estMot=false;
	node->taille=0;
	for(int i=0;i<26;i++)
		node->fils[i]=NULL;
	return node;
}

char buffer[1000],mot[100],cherche[26],trouve[26],lettres[26],indication[26];
bool tested[26][26], ispresent[26], foundcount[26], knownpresent[26];
char nick[100],st[100],moi[100],truc[100];

#define NBACCENTS (14)
char* accents[NBACCENTS]={"é","è","ê","ë","â","ä","à","ü","û","ù","î","ï","ô","ö"};
char sansaccents[NBACCENTS]={'e','e','e','e','a','a','a','u','u','u','i','i','o','o'};
int lenaccents[NBACCENTS]={-1+sizeof("é"),-1+sizeof("è"),-1+sizeof("ê"),-1+sizeof("ë"),
-1+sizeof("â"),-1+sizeof("ä"),-1+sizeof("à"),-1+sizeof("ü"),-1+sizeof("û"),-1+sizeof("ù"),
-1+sizeof("î"),-1+sizeof("ï"),-1+sizeof("ô"),-1+sizeof("ö")};

void ajoutTrie(Trie* u,char* s)
{
	for(int k=0;s[k]!='\0';k++)
	{
		u->taille++;
		int id=s[k]-'A';
		if(u->fils[id]==NULL)
			u->fils[id]=createTrie();
		u=u->fils[id];
	}
	u->taille++;
	u->estMot=true;
}

void libereTrie(Trie* u)
{
	for(int i=0;i<26;i++)
		if(u->fils[i]!=NULL)
			libereTrie(u->fils[i]);
	free(u);
}

bool estPresent(Trie* u,char* s)
{
	for(int k=0;s[k]!='\0';k++)
	{
		int id=s[k]-'A';
		if(0<=id && id<26 && u->fils[id]!=NULL)
			u=u->fils[id];
		else
			return false;
	}
	return u->estMot;
}

void piocheMot(Trie* u,char* s,int k)
{
	if(u==NULL || k>=u->taille)
		return;
	for(int i=0;i<26;i++)
	{
		if(u->fils[i]!=NULL && u->fils[i]->taille>k)
		{
			s[0]='A'+i;
			piocheMot(u->fils[i],s+1,k);
			return;
		}
		k-=(u->fils[i]!=NULL?u->fils[i]->taille:0);
	}
	s[0]='\0';
}

FILE *out;
int in;
Trie* racineTest,*racineChoix,*racinePropose;

typedef struct score_t
{
	char* nick;
	long long score;
	unsigned int count;
} score_t;

typedef struct pascal_score_t
{
	struct score_t* data;
	size_t length, max_length;
} pascal_score_t;

pascal_score_t* scores;

pascal_score_t* createScore()
{
	pascal_score_t* scores = malloc(sizeof(pascal_score_t));
	scores->length = 0;
	scores->max_length = 8;
	scores->data = malloc(scores->max_length * sizeof(score_t));
	return scores;
}

void appendScore(pascal_score_t* scores, long long x, int c){
	if(scores->length >= scores->max_length)
	{
		score_t* data = malloc(2*sizeof(score_t)*scores->max_length);
		memcpy(data, scores->data, sizeof(score_t)*scores->max_length);
		scores->max_length = scores->max_length*2;
		free(scores->data);
		scores->data = data;
	}
	scores->data[scores->length].nick=malloc(strlen(nick)+1);
	strcpy(scores->data[scores->length].nick, nick);
	scores->data[scores->length].score = x;
	scores->data[scores->length].count = c;
	scores->length++;
}

void addScore(pascal_score_t* scores, long long x, int c)
{
	for(int i = 0 ; i < scores->length ; ++i)
	{
		if(!strcmp(nick, scores->data[i].nick))
		{
			scores->data[i].score += x;
			scores->data[i].count += c;
			return;
		}
	}
	appendScore(scores, x, c);
}

pascal_score_t* loadScore(char* path)
{
	pascal_score_t* scores = createScore();
	FILE* scorefile = fopen(path, "r");
	if(!scorefile) return scores;
	while(fscanf(scorefile,"%s",nick)!=EOF)
	{
		long long score; unsigned int count;
		fscanf(scorefile,"%lld %u",&score,&count);
		appendScore(scores, score, count);
	}
	fclose(scorefile);
	return scores;
}

void saveScore(char* path, pascal_score_t* scores)
{
	FILE* scorefile=fopen(path,"w");
	for(int i = 0 ; i < scores->length ; ++i)
		fprintf(scorefile,"%s %lld %u\n",scores->data[i].nick,scores->data[i].score,scores->data[i].count);
	fclose(scorefile);
}

int cmp(const void* a, const void* b)
{
	long long v=((score_t*)b)->score-((score_t*)a)->score;
	if(v>0)
		return 1;
	else if(v<0)
		return -1;
	else return 0;
}

void printScore(pascal_score_t* scores)
{
	qsort(scores->data,scores->length,sizeof(score_t),cmp);
	for(int i = 0 ; i < scores->length && i < 10 ; ++i)
	{
		fprintf(out,"_%s: %lld(%u)",scores->data[i].nick,scores->data[i].score,scores->data[i].count);
		if(i < scores->length - 1 && i < 9) fprintf(out," | ");
		else fprintf(out,"\n");
	}
	fflush(out);
}

bool mygets(char* b,size_t sz,int fd)
{
	size_t i=0;
	char c='a';
	while(c!='\n' && i < sz)
	{
		if(read(fd,&c,sizeof(char))!=sizeof(char))
			return false;
		b[i++]=c;
	}
	b[i-1]='\0';
	return true;
}

void partie()
{
	srand(time(NULL));
	piocheMot(racineChoix,cherche,rand()%racineChoix->taille);
	int lenCherche=strlen(cherche);
	memset(tested,0,sizeof(tested));
	memset(ispresent,0,sizeof(ispresent));
	for(int i=0;cherche[i]!='\0';i++)
		ispresent[cherche[i]-'A']=true;
	memset(foundcount,0,sizeof(foundcount));
	memset(knownpresent,0,sizeof(knownpresent));
	fprintf(out,"Mot en %d lettres\n",lenCherche);
#ifdef DISCORD
	fprintf(out,"`");
#endif
	for(int i=0;cherche[i]!='\0';i++)
		fprintf(out,"ˍ");
#ifdef DISCORD
	fprintf(out,"`");
#endif
	fprintf(out,"\n");
	fflush(out);
	racinePropose=createTrie();
	while(1)
	{	
		if(!mygets(buffer,1000,in))
		{
			sleep(0.5);
			continue;
		}
		if(sscanf(buffer,"%s%s%s%s",st,nick,moi,mot)<4) continue;
		if(strncmp(moi,"botus",5)!=0)
			continue;
		for(int i=1;nick[i]!='\0';i++)
			nick[i-1]=(nick[i]!='>'?nick[i]:'\0');
#ifdef DISCORD
		char* ps=strrchr(nick,'_');
		if(ps!=NULL)
			*ps='\0';
#endif
		int k=0;
		for(int j=0;mot[j]!='\0';j++){
			bool flag=false;
			for(int idchar=0;idchar<NBACCENTS;idchar++)
				if(!strncmp(mot+j,accents[idchar],lenaccents[idchar]))
				{
					truc[k++]=sansaccents[idchar];
					j+=lenaccents[idchar]-1;
					flag=true;
					break;
				}
			if(!flag)
				truc[k++]=mot[j];
		}
		truc[k]='\0';
		strcpy(mot,truc);
		for(int i=0;mot[i]!='\0';i++)
			if(!isalpha(i))
				continue;
		if(mot[0]=='!'){
			if(strncmp(mot,"!indice",7)==0){
				int k;
				do{
					k=rand()%lenCherche;
				}while(trouve[k]);
				trouve[k]=true;
			}
			/*else if(strncmp(mot,"!suivant",8)==0){
				piocheMot(racineChoix,cherche,rand()%racineChoix->taille);
				lenCherche=strlen(cherche);
				fprintf(out,"Mot en %d lettres\n",lenCherche);
				memset(trouve,0,26);
				libereTrie(racinePropose);
				racinePropose=createTrie();
			}*/
			else if(strncmp(mot,"!stop",5)==0){
				break;
			}
			else if(strncmp(mot,"!help",5)==0){
				fprintf(out,"Les différentes commandes sont \"!indice\",\"!suivant\",\"!stop\"\n");
			}
			else if(strncmp(mot,"!scores",7)==0){
				printScore(scores);
			}
		}
		else if(strlen(mot)==lenCherche){
			for(int i=0;mot[i]!='\0';i++)
				mot[i]=toupper(mot[i]);
			if(estPresent(racineTest,mot))
			{
				if(!estPresent(racinePropose,mot))
				{
					int scoreGain=0;
					memset(lettres,0,26);
					int nbCorrectes=0;
					for(int i=0;mot[i]!='\0';i++)
						if(mot[i]==cherche[i])
						{
							if(!trouve[i])
							{
								trouve[i]=true;
								scoreGain+=5;
							}
							nbCorrectes++;
						}
						else
						{
							if(!trouve[i] && !tested[i][mot[i]-'A'] && ispresent[mot[i]-'A'])
							{
								tested[i][mot[i]-'A']=true;
								scoreGain+=1;
							}
							if(ispresent[mot[i]-'A'] && !knownpresent[mot[i]-'A'])
							{
								knownpresent[mot[i]-'A']=true;
								scoreGain+=1;
							}
							lettres[cherche[i]-'A']++;
						}
					memset(indication,'_',25);
#ifdef DISCORD
					fprintf(out,"`");
#else
					fprintf(out,"%c",0x02);
#endif
					for(int i=0;mot[i]!='\0';i++)
					{
						if(mot[i]==cherche[i])
						{
							//printf("\e[1;32m%c\e[0m",mot[i]);
							fprintf(out,"%c03%c%c",0x03,mot[i],0x03);
							indication[i]='^';
						}
						else if(lettres[mot[i]-'A']>0)
						{
							//printf("\e[1;31m%c\e[0m",mot[i]);
							fprintf(out,"%c05%c%c",0x03,mot[i],0x03);
							lettres[mot[i]-'A']--;
							indication[i]='~';
						}
						else
						{
							fprintf(out,"%c",mot[i]);
							if(!foundcount[mot[i]-'A'])
							{
								foundcount[mot[i]-'A']=true;
								scoreGain+=1;
							}
						}
					}
					if(nbCorrectes==lenCherche) scoreGain+=5;
#ifndef DISCORD
					fprintf(out,"%c (%d pts)\n",0x02,scoreGain);
#else
					fprintf(out,"` (%d pts)\n`",scoreGain);
					for(int i=0;mot[i]!='\0';i++)
					{
						fprintf(out,"%c",indication[i]);
					}
					fprintf(out,"` ");
#endif
					ajoutTrie(racinePropose,mot);
					addScore(scores,scoreGain,(nbCorrectes==lenCherche));
					if(nbCorrectes==lenCherche)
					{
						for(int i=0;i<lenCherche;i++)
							cherche[i]=tolower(cherche[i]);
						fprintf(out,"Bravo %s !\nDictionnaire : https://1mot.net/%s\n",nick,cherche);
						fflush(out);
						libereTrie(racinePropose);
						racinePropose=createTrie();
						memset(trouve,0,26);
						break;
					}
				}
				else
				{
					fprintf(out,"Mot déjà proposé ! (-3 pts)\n");
					addScore(scores,-3,0);
					fflush(out);
				}
			}
			else
			{
				fprintf(out,"Mot invalide ! (-10 pts)\n");
				addScore(scores,-10,0);
				fflush(out);
			}
		}
		else
		{
			fprintf(out,"Longueur incorrecte : %d\n",strlen(mot));
			fflush(out);
		}
#ifdef DISCORD
		fprintf(out,"`");
#endif
		for(int i=0;cherche[i]!='\0';i++)
			if(trouve[i])
				fprintf(out,"%c",cherche[i]);
			else
				fprintf(out,"ˍ");
#ifdef DISCORD
		fprintf(out,"`");
#endif
		fprintf(out,"\n");
		fflush(out);
	}
	saveScore(SAVEFILE,scores);
}

int main()
{
	setlocale(LC_ALL, "");
	out = fopen("in","w");
	fflush(out);
	in=open("out",O_RDONLY | O_NONBLOCK);
	assert(in!=-1);
	assert(out!=NULL);
	FILE *dico = fopen("dico.txt","r");
	racineTest=createTrie();
	while(fscanf(dico,"%s",buffer)!=EOF)
	{
		ajoutTrie(racineTest,buffer);
	}
	fclose(dico);
	dico = fopen("wordlist.txt","r");
	racineChoix=createTrie();
	while(fscanf(dico,"%s",buffer)!=EOF)
	{
		ajoutTrie(racineChoix,buffer);
	}
	fclose(dico);
	scores=loadScore(SAVEFILE);
	lseek(in,-1,SEEK_END);
	while(1)
	{
		if(mygets(buffer,1000,in))
		{
			if(strstr(buffer,"botus: !start")!=0||strstr(buffer,"botus: encore")!=0)
				partie();
			if(strstr(buffer,"botus: !scores")!=0)
				printScore(scores);
		}
		sleep(1);
	}
}
